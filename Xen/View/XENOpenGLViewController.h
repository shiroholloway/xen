//
//  XENOpenGLViewController.h
//  Xen Game Framework
//
//  Created by Robert Luis Hoover on 9/29/13.
//  Copyright (c) 2013 ars draconis. All rights reserved.
//

@import Cocoa;

#import "XENOpenGLViewControllerDelegate.h"
#import "XENOpenGLViewDelegate.h"

@interface XENOpenGLViewController : NSViewController <XENOpenGLViewDelegate>

@property (nonatomic, assign) id<XENOpenGLViewControllerDelegate> delegate;

@property (nonatomic, getter = isPaused) BOOL paused;

@property (nonatomic) BOOL pauseOnWillResignActive;
@property (nonatomic) BOOL resumeOnDidBecomeActive;

@property (nonatomic, readonly) NSTimeInterval timeSinceFirstResume;
@property (nonatomic, readonly) NSTimeInterval timeSinceLastResume;

@property (nonatomic, readonly) NSTimeInterval timeSinceLastDraw;
@property (nonatomic, readonly) NSTimeInterval timeSinceLastUpdate;

@property (nonatomic, readonly) NSInteger framesDisplayed;
@property (nonatomic, readonly) NSInteger framesPerSecond;
@property (nonatomic) NSInteger preferredFramesPerSecond;

- (void)glView:(XENOpenGLView *)view drawInRect:(NSRect)rect;

@end
