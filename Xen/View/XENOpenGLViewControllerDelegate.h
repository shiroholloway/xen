//
//  XENViewControllerDelegate.h
//  Xen Game Framework
//
//  Created by Robert Luis Hoover on 9/29/13.
//  Copyright (c) 2013 ars draconis. All rights reserved.
//

@import Foundation;

@class XENOpenGLViewController;

@protocol XENOpenGLViewControllerDelegate <NSObject>

@required
- (void)glViewControllerUpdate:(XENOpenGLViewController *)controller;

@optional
- (void)glViewController:(XENOpenGLViewController *)controller willPause:(BOOL)pause;

@end
