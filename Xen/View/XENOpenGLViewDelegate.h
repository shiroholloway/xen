//
//  XENViewDelegate.h
//  Xen Game Framework
//
//  Created by Robert Luis Hoover on 9/29/13.
//  Copyright (c) 2013 ars draconis. All rights reserved.
//

@import Foundation;

@class XENOpenGLView;

@protocol XENOpenGLViewDelegate <NSObject>

@required
- (void)glView:(XENOpenGLView *)view drawInRect:(NSRect)rect;

@optional
- (void)prepareOpenGL:(XENOpenGLView *)view;

@end
