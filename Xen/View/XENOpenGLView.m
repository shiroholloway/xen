//
//  XENOpenGLView.m
//  Xen Game Framework
//
//  Created by Robert Luis Hoover on 9/29/13.
//  Copyright (c) 2013 ars draconis. All rights reserved.
//

@import OpenGL.GL;

#import "XENOpenGLView.h"

// https://developer.apple.com/library/ios/documentation/GLkit/Reference/GLKView_ClassReference/Reference/Reference.html#//apple_ref/occ/instp/GLKView/delegate

@implementation XENOpenGLView

@dynamic drawableWidth, drawableHeight;

+ (NSOpenGLPixelFormat *)defaultPixelFormat
{
	// Creates a generic pixel format.
	NSOpenGLPixelFormatAttribute attributes[] = {
		NSOpenGLPFAAlphaSize, 8,
		NSOpenGLPFADepthSize, 16,
		NSOpenGLPFADoubleBuffer,
		0
	};
	
	return [[NSOpenGLPixelFormat alloc] initWithAttributes:attributes];
}

// -----------------------------------------------------------------------------
// Initialization
// -----------------------------------------------------------------------------
#pragma mark - Initialization

- (id)initWithFrame:(NSRect)frameRect
{
	return [self initWithFrame:frameRect context:nil];
}

- (id)initWithFrame:(NSRect)frameRect pixelFormat:(NSOpenGLPixelFormat *)format
{
	if (!format) format = [[self class] defaultPixelFormat];
	
	NSOpenGLContext *context = [[NSOpenGLContext alloc] initWithFormat:format shareContext:nil];
	
	self = [self initWithFrame:frameRect context:context];
	if (self)
	{
		[self setPixelFormat:format];
	}
	return self;
}

- (id)initWithFrame:(NSRect)frameRect context:(NSOpenGLContext *)context
{
	self = [super initWithFrame:frameRect];
	if (self)
	{
		if (!context)
		{
			NSOpenGLPixelFormat *format = [[self class] defaultPixelFormat];
			context = [[NSOpenGLContext alloc] initWithFormat:format shareContext:nil];
			[self setPixelFormat:format];
		}
		
		[self setOpenGLContext:context];
		[context setView:self];
		_enableSetNeedsDisplay = YES;
	}
	return self;
}

// -----------------------------------------------------------------------------
// Utility Methods
// -----------------------------------------------------------------------------
- (void)reshape
{
	CGLContextObj context = [self.openGLContext CGLContextObj];
	CGLLockContext(context);
	glViewport(0, 0, (GLsizei)self.drawableWidth, (GLsizei)self.drawableHeight);
	CGLUnlockContext(context);
	
	// We redraw when the window gets resized. This also keeps us from having a
	// blank window when we exit fullscreen.
	[self displayFrame:NSZeroRect];
}

- (void)prepareOpenGL
{
	if (_delegate && [_delegate respondsToSelector:@selector(prepareOpenGL:)])
	{
		[_delegate prepareOpenGL:self];
	}
}

// -----------------------------------------------------------------------------
// Accessors
// -----------------------------------------------------------------------------
#pragma mark - Accessors

- (NSImage *)snapshot
{
	// TODO: Write me!
	/* What
	GLint rowBytes = (int)self.drawableWidth * 4;
	uint8_t *buffer = calloc(rowBytes * self.drawableHeight, sizeof(uint8_t));
	NSOpenGLContext *context = self.openGLContext;
	GLuint framebuffer, renderbuffer;
	
	glGenFramebuffers(1, &framebuffer);
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, framebuffer);
	glGenRenderbuffers(1, &renderbuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, renderbuffer);
	glRenderbufferStorage(GL_RENDERBUFFER_EXT, GL_RGBA8, self.drawableWidth, self.drawableHeight);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_RENDERBUFFER_EXT, renderbuffer);
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER_EXT) != GL_FRAMEBUFFER_COMPLETE)
	{
		NSLog(@"Framebuffer object not created.");
		return nil;
	}
	
	[context makeCurrentContext];
	[context flushBuffer];
	
	glCopyTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, 0, 0, self.drawableWidth, self.drawableHeight, 0);
	
	[self deleteDrawable];
	[self bindDrawable];
	
	glBindFramebuffer(GL_FRAMEBUFFER_EXT, 0);
	glDeleteRenderbuffers(1, &renderbuffer);
	glDeleteFramebuffers(1, &framebuffer);
	
	
	NSBitmapImageRep *rep = [[NSBitmapImageRep alloc] initWithBitmapDataPlanes:&buffer
																	pixelsWide:self.drawableWidth
																	pixelsHigh:self.drawableHeight
																 bitsPerSample:8
															   samplesPerPixel:4
																	  hasAlpha:YES
																	  isPlanar:NO
																colorSpaceName:NSCalibratedRGBColorSpace
																   bytesPerRow:rowBytes
																  bitsPerPixel:32];
	
	NSImage *image = [[NSImage alloc] initWithSize:[self convertSizeToBacking:[self bounds].size]];
	[image addRepresentation:rep];
	return image;
	 */
	return nil;
}

- (NSInteger)drawableWidth
{
	NSSize size = [self convertSizeToBacking:[self bounds].size];
	return size.width;
}

- (NSInteger)drawableHeight
{
	NSSize size = [self convertSizeToBacking:[self bounds].size];
	return size.height;
}

// -----------------------------------------------------------------------------
// Drawing
// -----------------------------------------------------------------------------
#pragma mark - Drawing

- (void)bindDrawable
{
	[self.openGLContext setView:self];
}

- (void)deleteDrawable
{
	[self.openGLContext clearDrawable];
}

- (void)lockFocus
{
	[super lockFocus];
	
	if ([self.openGLContext view] != self)
	{
		[self.openGLContext setView:self];
	}
}

- (void)drawRect:(NSRect)dirtyRect
{
	if (_enableSetNeedsDisplay)
	{
		[self displayFrame:dirtyRect];
	}
}

- (void)displayFrame:(NSRect)dirtyRect
{
	if (_delegate)
	{
		[_delegate glView:self drawInRect:dirtyRect];
	}
}

@end
