//
//  XENOpenGLViewController.m
//  Xen Game Framework
//
//  Created by Robert Luis Hoover on 9/29/13.
//  Copyright (c) 2013 ars draconis. All rights reserved.
//

@import OpenGL.GL;

#import "XENOpenGLView.h"

#import "XENOpenGLViewController.h"

// https://developer.apple.com/library/ios/documentation/GLkit/Reference/GLKViewController_ClassRef/Reference/Reference.html#//apple_ref/occ/instp/GLKViewController/delegate

// -----------------------------------------------------------------------------
// Class Extension
// -----------------------------------------------------------------------------
@interface XENOpenGLViewController ()

@property (assign) CGDirectDisplayID mainDisplayID;
@property (assign) CVDisplayLinkRef displayLink;

@property (nonatomic) NSTimeInterval timeOfFirstResume;
@property (nonatomic) NSTimeInterval timeOfLastResume;
@property (nonatomic) NSTimeInterval timeOfLastUpdate;
@property (nonatomic) NSTimeInterval timeOfLastDraw;

- (CVReturn)_getFrameForTime:(const CVTimeStamp*)outputTime;
- (void)_updateDisplayLink;

@end

// -----------------------------------------------------------------------------
// Implementation
// -----------------------------------------------------------------------------
@implementation XENOpenGLViewController

@dynamic timeSinceFirstResume, timeSinceLastResume, timeSinceLastUpdate, timeSinceLastDraw;

// -----------------------------------------------------------------------------
// Initialization
// -----------------------------------------------------------------------------
#pragma mark - Initialization

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
	{
		_preferredFramesPerSecond = 60;
		_framesDisplayed = 0;
		_pauseOnWillResignActive = YES;
		_resumeOnDidBecomeActive = YES;
		
		NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
		[defaultCenter addObserver:self
						  selector:@selector(applicationDidBecomeActive:)
							  name:NSApplicationDidBecomeActiveNotification
							object:[NSApplication sharedApplication]];
		[defaultCenter addObserver:self
						  selector:@selector(applicationWillResignActive:)
							  name:NSApplicationWillResignActiveNotification
							object:[NSApplication sharedApplication]];
    }
    return self;
}

- (void)dealloc
{
	CVDisplayLinkStop(_displayLink);
	CVDisplayLinkRelease(_displayLink);
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

// -----------------------------------------------------------------------------
// Accessors
// -----------------------------------------------------------------------------
#pragma mark - Accessors

- (NSTimeInterval)timeSinceFirstResume
{
	return _timeOfFirstResume ? CFAbsoluteTimeGetCurrent() - _timeOfFirstResume : 0;
}

- (NSTimeInterval)timeSinceLastResume
{
	return _timeOfLastResume ? CFAbsoluteTimeGetCurrent() - _timeOfLastResume : 0;
}

- (NSTimeInterval)timeSinceLastUpdate
{
	return _timeOfLastUpdate ? CFAbsoluteTimeGetCurrent() - _timeOfLastUpdate : 0;
}

- (NSTimeInterval)timeSinceLastDraw
{
	return _timeOfLastDraw ? CFAbsoluteTimeGetCurrent() - _timeOfLastDraw : 0;
}

- (void)setPaused:(BOOL)flag
{
	if (flag == _paused) return;
		
	[self willChangeValueForKey:@"paused"];
	
	// Notify delegate.
	if (_delegate && [_delegate respondsToSelector:@selector(glViewController:willPause:)])
	{
		[_delegate glViewController:self willPause:flag];
	}
	
	// Start/stop Display Link.
	XENOpenGLView *glView = (XENOpenGLView *)self.view;
	if (flag)
	{
		if (CVDisplayLinkIsRunning(_displayLink))
		{
			glView.enableSetNeedsDisplay = YES;
			CVDisplayLinkStop(_displayLink);
		}
	}
	else
	{
		if (!CVDisplayLinkIsRunning(_displayLink))
		{
			glView.enableSetNeedsDisplay = NO;
			CVDisplayLinkStart(_displayLink);
		}
		
		_timeOfLastResume = CFAbsoluteTimeGetCurrent();
		if (!_timeOfFirstResume)
			_timeOfFirstResume = _timeOfLastResume;
	}
	
	// Actually change value.
	_paused = flag;
	
	[self didChangeValueForKey:@"paused"];
}


- (void)setView:(NSView *)view
{
	[super setView:view];
	
	XENOpenGLView *glView = (XENOpenGLView *)view;
	if ([view isKindOfClass:[XENOpenGLView class]] && !glView.delegate)
	{
		[glView setDelegate:self];
	}
	
	glView.enableSetNeedsDisplay = NO;
	[self _updateDisplayLink];
	[self prepareOpenGL:glView];
}

// -----------------------------------------------------------------------------
// Notifications
// -----------------------------------------------------------------------------
- (void)applicationDidBecomeActive:(NSNotification *)aNotification
{
	if (_resumeOnDidBecomeActive)
	{
		self.paused = NO;
	}
}

- (void)applicationWillResignActive:(NSNotification *)aNotification
{
	if (_pauseOnWillResignActive)
	{
		self.paused = YES;
	}
}

// -----------------------------------------------------------------------------
// DisplayLink Support
// -----------------------------------------------------------------------------
#pragma mark - DisplayLink Support

static CVReturn XENDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp* now, const CVTimeStamp* outputTime, CVOptionFlags flagsIn, CVOptionFlags* flagsOut, void* viewController)
{
	//NSLog(@"DisplayLink callback called.");
	return [(__bridge XENOpenGLViewController *)viewController _getFrameForTime:outputTime];
}

- (void)_updateDisplayLink
{
	XENOpenGLView *view = (XENOpenGLView *)self.view;
	
	NSWindow *window = [view window];
	CGDirectDisplayID displayID = [[[[window screen] deviceDescription] objectForKey:@"NSScreenNumber"] intValue];
	
	if (!_mainDisplayID)
	{
		// If this is the first time we're called, set up the display link.
		CVDisplayLinkCreateWithActiveCGDisplays(&_displayLink);
		CVDisplayLinkSetOutputCallback(_displayLink, &XENDisplayLinkCallback, (__bridge void *)self);
		CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(_displayLink,
														  [view.openGLContext CGLContextObj],
														  [view.pixelFormat CGLPixelFormatObj]);
		// Start the display link.
		CVDisplayLinkStart(_displayLink);

	}
	else if(_mainDisplayID != displayID)
	{
		// If we've been called before and we're on a different display, update
		// the link to the new display.
		CVDisplayLinkSetCurrentCGDisplay(_displayLink, displayID);
	}
	
	_mainDisplayID = displayID;
}

- (CVReturn)_getFrameForTime:(const CVTimeStamp*)outputTime
{
	// Find the current framerate.
	static NSInteger currentTick;
	NSInteger displayFramerate = ceil((double)outputTime->videoTimeScale / outputTime->videoRefreshPeriod);
	NSInteger tickLength = displayFramerate / _preferredFramesPerSecond;
	_framesDisplayed = displayFramerate / tickLength;
	
	if (!(currentTick++ % tickLength) && !_paused)
	{
		@autoreleasepool
		{
			_timeOfLastUpdate = CFAbsoluteTimeGetCurrent();
			
			dispatch_async(dispatch_get_main_queue(), ^{
				if (_delegate)
				{
					[_delegate glViewControllerUpdate:self];
				}
				else
				{
					[self update];
				}
			});
			
			_timeOfLastDraw = CFAbsoluteTimeGetCurrent();
			[(XENOpenGLView *)self.view displayFrame:NSZeroRect];
			_framesDisplayed++;
		}
	}
	
	if (currentTick >= 60) currentTick = 0;
	return kCVReturnSuccess;
}

// -----------------------------------------------------------------------------
// Setup
// -----------------------------------------------------------------------------
#pragma mark - Setup

- (void)prepareOpenGL:(XENOpenGLView *)view
{
	// Set up vertical sync.
	GLint swapInt = 1;
	[view.openGLContext setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
	
	// Set up clear color as black.
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
}

// -----------------------------------------------------------------------------
// Updating
// -----------------------------------------------------------------------------
#pragma mark - Updating

- (void)update
{
}

- (void)glView:(XENOpenGLView *)view drawInRect:(NSRect)rect
{
	// Clears view to black.
	NSOpenGLContext *context = view.openGLContext;
	CGLLockContext([context CGLContextObj]);
	[context makeCurrentContext];
		
	// Clear drawable.
    glClear(GL_COLOR_BUFFER_BIT);
	
	// Drawing complete.
	[context flushBuffer];
	CGLUnlockContext([context CGLContextObj]);

}

@end
