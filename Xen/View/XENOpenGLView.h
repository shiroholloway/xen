//
//  XENOpenGLView.h
//  Xen Game Framework
//
//  Created by Robert Luis Hoover on 9/29/13.
//  Copyright (c) 2013 ars draconis. All rights reserved.
//

@import Cocoa;

#import "XENOpenGLViewDelegate.h"

@interface XENOpenGLView : NSOpenGLView

@property (nonatomic, assign) IBOutlet id<XENOpenGLViewDelegate> delegate;

@property (nonatomic, assign) BOOL enableSetNeedsDisplay;

@property(nonatomic, readonly) NSInteger drawableWidth;
@property(nonatomic, readonly) NSInteger drawableHeight;

- (NSImage *)snapshot;
- (void)bindDrawable;
- (void)deleteDrawable;
- (void)displayFrame:(NSRect)dirtyRect;

@end
