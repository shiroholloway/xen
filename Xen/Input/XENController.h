//
//  XENController.h
//  Xen
//
//  Created by Robert Luis Hoover on 10/6/13.
//  Copyright (c) 2013 ars draconis. All rights reserved.
//

@import Foundation;
@import IOKit.hid;

@class XENControllerProfile;

// -----------------------------------------------------------------------------
// Keys
// -----------------------------------------------------------------------------

// Notifications
/**
 *	<#Description#>
 */
extern NSString * const XENControllerDidConnectNotification;
/**
 *	<#Description#>
 */
extern NSString * const XENControllerDidDisconnectNotification;

// Transport Types
extern NSString * const XENControllerTransportUSB;
extern NSString * const XENControllerTransportBluetooth;

// -----------------------------------------------------------------------------
// Type Definitions
// -----------------------------------------------------------------------------
/**
 *	<#Description#>
 */
typedef NS_ENUM(NSInteger, XENDevicePlayerIndex) {
	/**
	 *	<#Description#>
	 */
	XENDevicePlayerIndexUnset = -1,
	/**
	 *	<#Description#>
	 */
	XENDevicePlayerIndexOne = 0,
	/**
	 *	<#Description#>
	 */
	XENDevicePlayerIndexTwo,
	/**
	 *	<#Description#>
	 */
	XENDevicePlayerIndexThree,
	/**
	 *	<#Description#>
	 */
	XENDevicePlayerIndexFour
};


// -----------------------------------------------------------------------------
// Class Interface
// -----------------------------------------------------------------------------
/**
 *	<#Description#>
 */
@interface XENController : NSObject

// Determining Which Profiles Are Supported by a Device
@property (retain, readonly) XENControllerProfile *profile;

// Inspecting a Device
@property (readonly, copy) NSString *vendorName;
@property (readonly, copy) NSString *deviceName;

// Assigning a Player Index
@property (nonatomic) NSInteger playerIndex;

// Creating and Deallocating Objects
- (id)init NS_UNAVAILABLE;
- (instancetype)initWithHIDDevice:(IOHIDDeviceRef)aDevice;

// Discovering Devices
+ (void)startWirelessControllerDiscoveryWithCompletionHandler:(void(^)(void))completionHandler;
+ (void)stopWirelessControllerDiscovery;
+ (NSArray *)controllers;

+ (NSArray *)controllersSupportingProfile:(Class)profile;

@end
