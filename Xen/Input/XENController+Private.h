//
//  XENController+Private.h
//  Xen
//
//  Created by Robert Luis Hoover on 11/30/13.
//  Copyright (c) 2013 ars draconis. All rights reserved.
//


// This is kind of ugly. As of right now, both XENHIDManager and XENControllerProfile
// make use of our IOHIDDeviceRef, but I'd rather not expose that in the publid
// interface. Wish @package and friends worked with @properties.

// -----------------------------------------------------------------------------
// Class Extension
// -----------------------------------------------------------------------------
@interface XENController ()

@property (assign, readonly) IOHIDDeviceRef deviceRef;

@end
