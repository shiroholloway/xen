//
//  XENMouse.h
//  Xen
//
//  Created by Robert Luis Hoover on 10/20/13.
//  Copyright (c) 2013 ars draconis. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "XENControllerProfile.h"

@class XENController, XENControllerElement;

@interface XENMouse : XENControllerProfile

typedef void (^XENMouseValueChangedHandler)(XENController *mouse, XENControllerElement *element);
@property (copy) XENMouseValueChangedHandler valueChangedHandler;

@end
