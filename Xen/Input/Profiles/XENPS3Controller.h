//
//  XENPS3Controller.h
//  Xen
//
//  Created by Robert Luis Hoover on 11/25/13.
//  Copyright (c) 2013 ars draconis. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "XENControllerProfile.h"

@class XENController;
@class XENControllerElement, XENControllerAxisInput, XENControllerButtonInput, XENControllerDirectionPad;

@interface XENPS3Controller : XENControllerProfile

typedef void (^XENPS3ControllerValueChangedHandler)(XENController *controller, XENControllerElement *element);
@property (copy) XENPS3ControllerValueChangedHandler valueChangedHandler;

@property (readonly) XENControllerDirectionPad *dpad;

@property (readonly) XENControllerDirectionPad *leftThumbstick;
@property (readonly) XENControllerButtonInput *L3;
@property (readonly) XENControllerDirectionPad *rightThumbstick;
@property (readonly) XENControllerButtonInput *R3;

@property (readonly) XENControllerButtonInput *L1;
@property (readonly) XENControllerButtonInput *L2;

@property (readonly) XENControllerButtonInput *R1;
@property (readonly) XENControllerButtonInput *R2;

@property (readonly) XENControllerButtonInput *triangleButton;
@property (readonly) XENControllerButtonInput *squareButton;
@property (readonly) XENControllerButtonInput *circleButton;
@property (readonly) XENControllerButtonInput *XButton;

@property (readonly) XENControllerButtonInput *startButton;
@property (readonly) XENControllerButtonInput *selectButton;
@property (readonly) XENControllerButtonInput *homeButton;

@end
