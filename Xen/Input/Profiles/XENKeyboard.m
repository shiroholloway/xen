//
//  XENKeyboard.m
//  Xen
//
//  Created by Robert Luis Hoover on 10/20/13.
//  Copyright (c) 2013 ars draconis. All rights reserved.
//

#import <IOKit/hid/IOHIDUsageTables.h>
#import "XENKeyboard.h"

@implementation XENKeyboard

+ (NSDictionary *)matchingDictionary
{
	return @{XENControllerUsagePageKey:	@(kHIDPage_GenericDesktop),
			 XENControllerUsageKey:		@(kHIDUsage_GD_Keyboard)};
}


@end
