//
//  XENPS3Controller.m
//  Xen
//
//  Created by Robert Luis Hoover on 11/25/13.
//  Copyright (c) 2013 ars draconis. All rights reserved.
//

#import <IOKit/hid/IOHIDLib.h>
#import "XENPS3Controller.h"



@implementation XENPS3Controller

+ (NSDictionary *)matchingDictionary
{
	/*
	CFDictionarySetValue(result, CFSTR(kIOHIDProductKey), CFSTR("PLAYSTATION(R)3 Controller"));
	CFDictionarySetValue(result, CFSTR(kIOHIDManufacturerKey), CFSTR("Sony"));
	// IDEA: We may want to remove the next line as a controller can also appear as a USB device when plugged in.
	CFDictionarySetValue(result, CFSTR(kIOHIDTransportKey), CFSTR("Bluetooth"));
	*/

	return @{XENControllerUsagePageKey: @(kHIDPage_GenericDesktop),
			 XENControllerUsageKey: @(kHIDUsage_GD_Joystick),
			 XENControllerVendorIDKey: @(0x054C),
			 XENControllerProductIDKey: @(0x0268)};
}

- (void)input:(IOHIDValueRef)value
{
	IOHIDElementRef element = IOHIDValueGetElement(value);
	IOHIDElementCookie cookie = IOHIDElementGetCookie(element);
	NSUInteger usage = IOHIDElementGetUsage(element);
	NSUInteger usagePage = IOHIDElementGetUsagePage(element);
	NSInteger valueValue = 0;
	NSInteger valueLength = IOHIDValueGetLength(value);
	const uint8_t *bytes = IOHIDValueGetBytePtr(value);
	
	// Keep from crashing. Seems to be due to constant value changes on cookie
	// 0x1E. Accelerometer data?
	if (valueLength > 2)
	{
		NSLog(@"Value change. Length %li. Cookie: %08X, usage: %04lX:%04lX. Raw output follows...", valueLength, cookie, (unsigned long)usagePage, (unsigned long)usage);
		for (int i = 0; i < valueLength; i++)
			printf("\t Byte %i: %0X \n", i, bytes[i]);
		return;
	}
	
	valueValue = IOHIDValueGetIntegerValue(value);
	
	// TODO: Check cookie values.
	// This was reverse engineered from a DualShock 3. However, older SIXAXIS
	// controllers used different usage page/usage pairs. Cookie values appear
	// to have remained the same, however, and are therefore more reliable since
	// there is no other way to distinguish between the two. Not to mention
	// avoiding duplicate effort for two otherwise similar controllers...

	if (usagePage == 0x0001)
	{
		// Process thumbstick input.
		if (usage == 0x0030)
		{
			NSLog(@"Left Stick X axis input. Value: %lX", (long)valueValue);
		}
		else if (usage == 0x0031)
		{
			NSLog(@"Left Stick Y axis input. Value: %lX", (long)valueValue);
		}
		else if (usage == 0x0032)
		{
			NSLog(@"Right Stick X axis input. Value: %lX", (long)valueValue);
		}
		else if (usage == 0x0035)
		{
			NSLog(@"Right Stick Y axis input. Value: %lX", (long)valueValue);
		}
		
	}
	
	if (usagePage == 0x0009 && valueValue)
	{
		// Process button input.
		if (usage == 0x0001)
		{
			NSLog(@"Select button pressed.");
		}
		else if (usage == 0x0002)
		{
			NSLog(@"L3 pressed.");
		}
		else if (usage == 0x0003)
		{
			NSLog(@"R3 pressed.");
		}
		else if (usage == 0x0004)
		{
			NSLog(@"Start button pressed.");
		}
		else if (usage == 0x0005)
		{
			NSLog(@"Up pressed.");
		}
		else if (usage == 0x0006)
		{
			NSLog(@"Right pressed.");
		}
		else if (usage == 0x0007)
		{
			NSLog(@"Down pressed.");
		}
		else if (usage == 0x0008)
		{
			NSLog(@"Left pressed.");
		}
		else if (usage == 0x0009)
		{
			NSLog(@"L2 pressed.");
		}
		else if (usage == 0x000A)
		{
			NSLog(@"R2 pressed.");
		}
		else if (usage == 0x000B)
		{
			NSLog(@"L1 pressed.");
		}
		else if (usage == 0x000C)
		{
			NSLog(@"R1 pressed.");
		}
		else if (usage == 0x000D)
		{
			NSLog(@"Triangle pressed.");
		}
		else if (usage == 0x000E)
		{
			NSLog(@"Circle pressed.");
		}
		else if (usage == 0x000F)
		{
			NSLog(@"X pressed.");
		}
		else if (usage == 0x0010)
		{
			NSLog(@"Square pressed.");
		}
		else if (usage == 0x0011)
		{
			NSLog(@"PS button pressed.");
		}
	}
	
	
}


@end
