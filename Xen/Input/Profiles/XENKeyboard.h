//
//  XENKeyboard.h
//  Xen
//
//  Created by Robert Luis Hoover on 10/20/13.
//  Copyright (c) 2013 ars draconis. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "XENControllerProfile.h"

@class XENController, XENControllerElement;

@interface XENKeyboard : XENControllerProfile

typedef void (^XENKeyboardValueChangedHandler)(XENKeyboard *keyboard, XENControllerElement *element);
@property (copy) XENKeyboardValueChangedHandler valueChangedHandler;


@end
