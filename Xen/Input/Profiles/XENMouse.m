//
//  XENMouse.m
//  Xen
//
//  Created by Robert Luis Hoover on 10/20/13.
//  Copyright (c) 2013 ars draconis. All rights reserved.
//

#import <IOKit/hid/IOHIDUsageTables.h>
#import "XENMouse.h"

@implementation XENMouse

+ (NSDictionary *)matchingDictionary
{
	return @{XENControllerUsagePageKey: @(kHIDPage_GenericDesktop),
			 XENControllerUsageKey: @(kHIDUsage_GD_Mouse)};
}


@end
