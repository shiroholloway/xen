//
//  XENHIDManager.h
//  Xen
//
//  Created by Robert Luis Hoover on 10/8/13.
//  Copyright (c) 2013 ars draconis. All rights reserved.
//

// This whole thing needs to be refactored.
// We have to create our device instances via the callbacks and simply update
// an array of all attached devices that we return whenever devices are asked for.

@import Foundation;
@import IOKit.hid;

#import "XENHIDManager.h"

#import "XENController.h"
#import "XENController+Private.h"
#import "XENControllerProfile.h"


// -----------------------------------------------------------------------------
// Constants
// -----------------------------------------------------------------------------

// Notifications
NSString * const XENDeviceDidConnectNotification = @"XENDeviceDidConnectNotification";
NSString * const XENDeviceDidDisconnectNotification = @"XENDeviceDidDisconnectNotification";

// -----------------------------------------------------------------------------
// Class Extension
// -----------------------------------------------------------------------------
@interface XENHIDManager ()

@property (assign) IOHIDManagerRef manager;
@property (readonly, strong) NSMutableArray *devices;

- (XENController *)deviceMatchingHIDDevice:(IOHIDDeviceRef)anIOHIDDeviceRef;

@end


// -----------------------------------------------------------------------------
// Callback Functions
// -----------------------------------------------------------------------------
static void XENHIDDeviceMatchingCallback(void *inContext, IOReturn inResult, void *inSender, IOHIDDeviceRef inIOHIDDevice)
{
	if (inResult == kIOReturnSuccess)
	{
		XENHIDManager *manager = (__bridge XENHIDManager *)inContext;
		XENController *newDevice = [[XENController alloc] initWithHIDDevice:inIOHIDDevice];
		if (!newDevice) return;
		
		[manager.devices addObject:newDevice];

		[[NSNotificationCenter defaultCenter] postNotificationName:XENDeviceDidConnectNotification
															object:newDevice];
		NSLog(@"Device connected: %@", newDevice);
	}
	else
	{
		NSLog(@"Device matching callback returned error code %i.", inResult);
	}
}

static void XENHIDDeviceRemovalCallback(void *inContext, IOReturn inResult, void *inSender, IOHIDDeviceRef inIOHIDDevice)
{
	if (inResult == kIOReturnSuccess)
	{
		XENHIDManager *manager = (__bridge XENHIDManager *)inContext;
		XENController *device = [manager deviceMatchingHIDDevice:inIOHIDDevice];
		if (!device) return;
		
		[[NSNotificationCenter defaultCenter] postNotificationName:XENDeviceDidDisconnectNotification
															object:device];
		
		[manager.devices removeObject:device];
		NSLog(@"Device disconnected: %@", device);
	}
	else
	{
		NSLog(@"Device removal callback returned error code %i.", inResult);
	}
}


// -----------------------------------------------------------------------------
// Implementation
// -----------------------------------------------------------------------------
@implementation XENHIDManager


// Getting the Shared Instance -------------------------------------------------
#pragma mark - Getting the Shared Instance

+ (XENHIDManager *)defaultManager
{
	static XENHIDManager *sharedInstance;
	static dispatch_once_t onceToken;
	
	dispatch_once(&onceToken, ^{
		sharedInstance = [[XENHIDManager alloc] init];
	});
	
	NSAssert(sharedInstance, @"Could not allocate a shared instance of XENHIDManager.");
	
	return sharedInstance;
}


// Creating and Deallocating Objects -------------------------------------------
#pragma mark - Creating and Deallocating Objects

- (id)init
{
	return [self initWithMatchingDictionary:NULL];
}

- (id)initWithMatchingDictionary:(CFMutableDictionaryRef)matchingDictionary
{
	self = [super init];
	if (self)
	{
		_devices = [NSMutableArray array];
		_manager = IOHIDManagerCreate(kCFAllocatorDefault, kIOHIDOptionsTypeNone);
		
		if (!_manager || CFGetTypeID(_manager) != IOHIDManagerGetTypeID())
		{
			NSLog(@"Could not create a valid IOHIDManager.");
			if (_manager) CFRelease(_manager);
			return nil;
		}
		
		IOHIDManagerSetDeviceMatching(_manager, matchingDictionary);
		
		// Register callbacks.
		IOHIDManagerRegisterDeviceMatchingCallback(_manager, XENHIDDeviceMatchingCallback, (__bridge void *)self);
		IOHIDManagerRegisterDeviceRemovalCallback(_manager, XENHIDDeviceRemovalCallback, (__bridge void *)self);
		IOHIDManagerScheduleWithRunLoop(_manager, CFRunLoopGetCurrent(), kCFRunLoopDefaultMode);
		
		// Open HID Manager.
		IOReturn returnCode = IOHIDManagerOpen(_manager, kIOHIDOptionsTypeNone);
		if (returnCode != kIOReturnSuccess)
		{
			NSLog(@"Could not open IOHIDManager.");
			IOHIDManagerUnscheduleFromRunLoop(_manager, CFRunLoopGetCurrent(), kCFRunLoopDefaultMode);
			CFRelease(_manager);
			return nil;
		}
	}
	return self;
}

- (void)dealloc
{
	if (_manager)
	{
		IOHIDManagerUnscheduleFromRunLoop(_manager, CFRunLoopGetCurrent(), kCFRunLoopDefaultMode);
		IOHIDManagerClose(_manager, kIOHIDOptionsTypeNone);
		CFRelease(_manager);
		_manager = NULL;
	}
}


// Getting Connected Devices ---------------------------------------------------
#pragma mark - Getting Connected Devices

- (NSArray *)allDevices
{
#if 0
	// Return all HID devices.
	NSMutableArray *result = [NSMutableArray array];
	NSSet *devices = (NSSet *)CFBridgingRelease(IOHIDManagerCopyDevices(_manager));
	
	for (id currentDevice in devices)
	{
		XENController *device = [[XENController alloc] initWithHIDDevice:(IOHIDDeviceRef)currentDevice];
		if (device) [result addObject:device];
	}
	
	return [result copy];
#endif
	return [_devices copy];
}

- (NSArray *)devicesMatchingDictionary:(NSDictionary *)matchingDictionary;
{
	// Return only devices matching a specific criteria.
	NSMutableArray *allDevices = [self.devices mutableCopy];
	NSMutableArray *devicesToRemove = [NSMutableArray array];
	
	// Filter out devices that don't match.
	for (XENController *device in allDevices)
	{
		NSDictionary *classDictionary = [[device.profile class] matchingDictionary];
		if (![classDictionary isEqualToDictionary:matchingDictionary])
			[devicesToRemove addObject:device];
	}
	
	[allDevices removeObjectsInArray:devicesToRemove];
	return [allDevices copy];
}

- (XENController *)deviceMatchingHIDDevice:(IOHIDDeviceRef)anIOHIDDeviceRef
{
	// Return the device that wraps a specific IOHIDDeviceRef.
	for (XENController *device in _devices)
	{
		if (device.deviceRef == anIOHIDDeviceRef)
			return device;
	}
	
	return nil;
}

@end
