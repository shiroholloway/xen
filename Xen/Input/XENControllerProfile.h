//
//  XENControllerProfile.h
//  Xen
//
//  Created by Robert Luis Hoover on 12/1/13.
//  Copyright (c) 2013 ars draconis. All rights reserved.
//

@import Foundation;
@import IOKit.hid;

@class XENController;


// -----------------------------------------------------------------------------
// Constants
// -----------------------------------------------------------------------------

// Matching Dictionary Keys
extern NSString * const XENControllerProductIDKey;
extern NSString * const XENControllerVendorIDKey;
extern NSString * const XENControllerUsagePairsKey;
extern NSString * const XENControllerUsagePageKey;
extern NSString * const XENControllerUsageKey;


// -----------------------------------------------------------------------------
// Interface
// -----------------------------------------------------------------------------
@interface XENControllerProfile : NSObject

@property (assign, readonly) XENController *device;

// Creating and Deallocating Objects
- (id)initWithDevice:(XENController *)aDevice;

// Getting a Matching Dictionary
+ (NSDictionary *)matchingDictionary;

// Saving a Snapshot
- (id)saveSnapshot;

// Processing Input (should be private)
- (void)input:(IOHIDValueRef)value;

@end
