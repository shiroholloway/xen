//
//  XENControllerButtonInput.h
//  Xen
//
//  Created by Robert Luis Hoover on 10/13/13.
//  Copyright (c) 2013 ars draconis. All rights reserved.
//

#import "XENControllerElement.h"

@class XENControllerButtonInput;
typedef void (^XENControllerButtonValueChangeHandler)(XENControllerButtonInput *button, float value, BOOL pressed);

@interface XENControllerButtonInput : XENControllerElement

// Reading the Button's Input
@property (readonly, getter = isPressed) BOOL pressed;
@property (readonly) float value;

// Receiving Notifications When the Button's Value Changes
@property (copy) XENControllerButtonValueChangeHandler valueChangedHandler;

@end
