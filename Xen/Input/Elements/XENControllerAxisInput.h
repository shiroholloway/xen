//
//  XENControllerAxisInput.h
//  Xen
//
//  Created by Robert Luis Hoover on 10/13/13.
//  Copyright (c) 2013 ars draconis. All rights reserved.
//

#import "XENControllerElement.h"

@class XENControllerAxisInput;
typedef void (^XENControllerAxisValueChangedHandler)(XENControllerAxisInput *axis, float value);

@interface XENControllerAxisInput : XENControllerElement

@property (readonly) float value;

@property (copy) XENControllerAxisValueChangedHandler valueChangedHandler;

@end
