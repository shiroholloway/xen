//
//  XENControllerDirectionPad.h
//  Xen
//
//  Created by Robert Luis Hoover on 10/20/13.
//  Copyright (c) 2013 ars draconis. All rights reserved.
//

#import "XENControllerElement.h"

@class XENControllerDirectionPad, XENControllerButtonInput, XENControllerAxisInput;
typedef void (^XENControllerDirectionPadValueChangedHandler)(XENControllerDirectionPad *dpad, float xValue, float yValue);


@interface XENControllerDirectionPad : XENControllerElement

@property (readonly) XENControllerAxisInput *xAxis;
@property (readonly) XENControllerAxisInput *yAxis;

@property (readonly) XENControllerButtonInput *up;
@property (readonly) XENControllerButtonInput *down;
@property (readonly) XENControllerButtonInput *left;
@property (readonly) XENControllerButtonInput *right;

@property (copy) XENControllerDirectionPadValueChangedHandler valueChangedHandler;

@end
