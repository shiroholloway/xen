//
//  XENControllerElement.h
//  Xen
//
//  Created by Robert Luis Hoover on 10/6/13.
//  Copyright (c) 2013 ars draconis. All rights reserved.
//

@import Foundation;

/**
 *	A XENControllerElement represents a measurable value on a controller profile.
 *	Typically, elements are mapped to physical controls on a controller. When a
 *	player manipulates those controls, the element object' value changes. This
 *	class is never instantiated directly. Instead, subclasses that represent
 *	the different kinds of elements are instantiated.
 */
@interface XENControllerElement : NSObject

/**
 *	If the value is YES, then the value properties defined by element subclasses
 *	can return a range of possible values, from minimum to maximum. If the value
 *	is NO, then the element's value properties only provide discrete values,
 *	typically 0 if off, or 1 if on.
 */
@property (readonly, getter = isAnalog) BOOL analog;

/**
 *	If the element is part of another element, this property holds the parent
 *	element. Otherwise, it holds nil.
 */
@property (weak, readonly) XENControllerElement *collection;

@end
