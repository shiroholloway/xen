//
//  XENController.m
//  Xen
//
//  Created by Robert Luis Hoover on 10/6/13.
//  Copyright (c) 2013 ars draconis. All rights reserved.
//

// See this technote
// https://developer.apple.com/library/mac/technotes/tn2187/_index.html
// Also this
// https://developer.apple.com/library/Mac/documentation/DeviceDrivers/Conceptual/HID/new_api_10_5/tn2187.html
// Check this out:
// http://www.signal11.us/oss/hidapi/

#import "XENController.h"
#import "XENController+Private.h"

#import "XENHIDManager.h"
#import "XENControllerProfile.h"


// -----------------------------------------------------------------------------
// Constants
// -----------------------------------------------------------------------------

// Transport Keys
NSString * const XENControllerTransportUSB = @"USB";
NSString * const XENControllerTransportBluetooth = @"Bluetooth";


// -----------------------------------------------------------------------------
// Callback Functions
// -----------------------------------------------------------------------------
static void XENHIDDeviceInputValueCallback(void *inContext, IOReturn inResult, void *inSender, IOHIDValueRef inIOHIDValueRef)
{
	XENControllerProfile *profile = (__bridge XENControllerProfile *)inContext;
	[profile input:inIOHIDValueRef];
}

// -----------------------------------------------------------------------------
// Implementation
// -----------------------------------------------------------------------------
@implementation XENController


// Creating and Deallocating Objects -------------------------------------------
#pragma mark - Creating and Deallocating Objects

- (instancetype)initWithHIDDevice:(IOHIDDeviceRef)aDevice
{
	NSAssert(aDevice, @"Someone attempted to initialize a XENController with a null IOHIDDeviceRef.");
	
	self = [super init];
	if (self)
	{
		_deviceRef = aDevice;
		CFRetain(_deviceRef);
		
		// Initialize our profile.
		_profile = [[XENControllerProfile alloc] initWithDevice:self];
		if (!_profile)
		{
			CFRelease(_deviceRef);
			_deviceRef = NULL;
			return nil;
		}
		
		// Initialize instance variables.
		_vendorName = (NSString *)CFBridgingRelease(IOHIDDeviceGetProperty(_deviceRef, CFSTR(kIOHIDManufacturerKey)));
		_deviceName = (NSString *)CFBridgingRelease(IOHIDDeviceGetProperty(_deviceRef, CFSTR(kIOHIDProductKey)));
		_playerIndex = XENDevicePlayerIndexUnset;
		
		// Register input value callback.
		IOHIDDeviceRegisterInputValueCallback(_deviceRef, &XENHIDDeviceInputValueCallback, (__bridge void *)_profile);
		IOHIDDeviceScheduleWithRunLoop(_deviceRef, CFRunLoopGetCurrent(), kCFRunLoopDefaultMode);
		
		// Open the device.
		IOReturn returnCode = IOHIDDeviceOpen(_deviceRef, kIOHIDOptionsTypeNone);
		if (returnCode != kIOReturnSuccess)
		{
			NSLog(@"Could not open IOHIDDeviceRef.");
			IOHIDDeviceUnscheduleFromRunLoop(_deviceRef, CFRunLoopGetCurrent(), kCFRunLoopDefaultMode);
			CFRelease(_deviceRef);
			_deviceRef = NULL;
			return nil;
		}
	}
	
	return self;
}

- (void)dealloc
{
	if (_deviceRef)
	{
		IOHIDDeviceUnscheduleFromRunLoop(_deviceRef, CFRunLoopGetCurrent(), kCFRunLoopDefaultMode);
		IOHIDDeviceClose(_deviceRef, kIOHIDOptionsTypeNone);
		CFRelease(_deviceRef);
		_deviceRef = NULL;
	}
}


// Describing Objects ----------------------------------------------------------
#pragma mark - Describing Objects
- (NSString *)description
{
	NSNumber *locationID = (NSNumber *)CFBridgingRelease(IOHIDDeviceGetProperty(_deviceRef, CFSTR(kIOHIDLocationIDKey)));
	NSString *transport = (NSString *)CFBridgingRelease(IOHIDDeviceGetProperty(_deviceRef, CFSTR(kIOHIDTransportKey)));

	return [NSString stringWithFormat:@"XENController: %p { \n\t Product: %@ \n\t Manufacturer: %@ \n\t Transport: %@ \n\t IOHIDDeviceRef: %p \n\t Location ID: %08lX \n\t Profile: %@ }", self, self.deviceName, self.vendorName, transport, _deviceRef, [locationID longValue], _profile];
}


// Assigning a Player Index ----------------------------------------------------
- (void)setPlayerIndex:(NSInteger)playerIndex
{
	[self willChangeValueForKey:@"playerIndex"];
	
	// TODO: Propogate this to the controller LEDs.
	// IDEA: We may just want to call in to a private method on XENControllerProfile.
	
	_playerIndex = playerIndex;
	[self didChangeValueForKey:@"playerIndex"];
}


// Finding Devices -------------------------------------------------------------
+ (NSArray *)controllers
{
	return [[XENHIDManager defaultManager] allDevices];
}

+ (NSArray *)controllersSupportingProfile:(Class)profile
{
	XENHIDManager *manager = [XENHIDManager defaultManager];
	NSDictionary *matchingDictionary = [profile matchingDictionary];
	return [manager devicesMatchingDictionary:matchingDictionary];
}

+ (void)startWirelessControllerDiscoveryWithCompletionHandler:(void (^)(void))completionHandler
{
	NSLog(@"%s is not yet implemented in %@", __PRETTY_FUNCTION__, NSStringFromClass([self class]));
}

+ (void)stopWirelessControllerDiscovery
{
	NSLog(@"%s is not yet implemented in %@", __PRETTY_FUNCTION__, NSStringFromClass([self class]));
}


@end
