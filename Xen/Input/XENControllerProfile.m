//
//  XENControllerProfile.m
//  Xen
//
//  Created by Robert Luis Hoover on 12/1/13.
//  Copyright (c) 2013 ars draconis. All rights reserved.
//

@import ObjectiveC.runtime;
@import IOKit.hid;

#import "XENControllerProfile.h"

#import "XENController.h"
#import "XENController+Private.h"


// -----------------------------------------------------------------------------
// Constants
// -----------------------------------------------------------------------------

// Matching Dictionary Keys ----------------------------------------------------
NSString * const XENControllerProductIDKey = @"ProductID";
NSString * const XENControllerVendorIDKey = @"VendorID";
NSString * const XENControllerUsagePairsKey = @"DeviceUsagePairs";
NSString * const XENControllerUsagePageKey = @"DeviceUsagePage";
NSString * const XENControllerUsageKey = @"DeviceUsage";


// -----------------------------------------------------------------------------
// Implementation
// -----------------------------------------------------------------------------
@implementation XENControllerProfile

// Creating and Deallocating Objects -------------------------------------------
#pragma mark - Creating and Deallocating Objects

- (id)initWithDevice:(XENController *)aDevice
{
	NSAssert(aDevice, @"Someone attempted to initialize a XENController with a nil XENController.");
	
	// We're a placeholder.
	self = nil;
	
	// Return an instance of the REAL killer!
	Class profileClass = [XENControllerProfile profileClassForDeviceRef:aDevice.deviceRef];
	self = [[profileClass alloc] init];
	
	if (self)
	{
		_device = aDevice;
	}
	return self;

}


// Getting a Matching Dictionary -----------------------------------------------
#pragma mark - Getting a Matching Dictionary

+ (NSDictionary *)matchingDictionary
{
	NSLog(@"%s is not yet implemented in %@", __PRETTY_FUNCTION__, NSStringFromClass([self class]));
	return nil;
}


// Saving a Snapshot -----------------------------------------------------------
#pragma mark - Saving a Snapshot

- (id)saveSnapshot
{
	NSLog(@"%s is not yet implemented in %@.", __PRETTY_FUNCTION__, NSStringFromClass([self class]));
	return nil;
}


// Processing Input ------------------------------------------------------------
#pragma mark - Processing Input

- (void)input:(IOHIDValueRef)value
{
	NSLog(@"%s is not yet implemented in %@.", __PRETTY_FUNCTION__, NSStringFromClass([self class]));
}


// Private Methods -------------------------------------------------------------
#pragma mark - Private Methods

+ (NSArray *)subclassStrings
{
	// There's magic here.
	static NSMutableArray *classes;
	static dispatch_once_t onceToken;
	
	// Don't really expect profile classes to be added at run time.
	dispatch_once(&onceToken, ^{
		classes = [NSMutableArray array];
		int classCount = objc_getClassList(NULL, 0);
		Class *classList = NULL;
		
		if (classCount)
		{
			classList = (Class *)realloc(classList, sizeof(Class) * classCount);
			classCount = objc_getClassList(classList, classCount);
		}
		
		for (int i = 0; i < classCount; i++)
		{
			Class superClass = classList[i];
			
			do {
				superClass = class_getSuperclass(superClass);
				
				if (superClass == self)
				{
					[classes addObject:NSStringFromClass(classList[i])];
					break;
				}
			} while (superClass);
		}
		
		free(classList);
		
	});
	
	return [classes copy];
}

+ (Class)profileClassForDeviceRef:(IOHIDDeviceRef)aDevice
{
	if (!aDevice) return Nil;
	
	// Get our device properties.
	NSNumber *deviceProductID = (NSNumber *)CFBridgingRelease(IOHIDDeviceGetProperty(aDevice, CFSTR(kIOHIDProductIDKey)));
	NSNumber *deviceVendorID = (NSNumber *)CFBridgingRelease(IOHIDDeviceGetProperty(aDevice, CFSTR(kIOHIDVendorIDKey)));
	NSNumber *devicePrimaryUsagePage = (NSNumber *)CFBridgingRelease(IOHIDDeviceGetProperty(aDevice, CFSTR(kIOHIDPrimaryUsagePageKey)));
	NSNumber *devicePrimaryUsage = (NSNumber *)CFBridgingRelease(IOHIDDeviceGetProperty(aDevice, CFSTR(kIOHIDPrimaryUsageKey)));
	//NSArray *deviceUsagePairs = (NSArray *)CFBridgingRelease(IOHIDDeviceGetProperty(aDevice, CFSTR(kIOHIDDeviceUsagePairsKey)));
	
	// Loop through each device class, asking for its matching dictionary.
	for (NSString *subclass in [self subclassStrings])
	{
		BOOL match = YES;
		Class deviceClass = NSClassFromString(subclass);
		NSDictionary *matchingDictionary = [deviceClass matchingDictionary];
		
		// Check the device's properties against the matching dictionary.
		for (id key in [matchingDictionary allKeys])
		{
			if (key == XENControllerProductIDKey)
			{
				NSNumber *productID = matchingDictionary[key];
				if ([deviceProductID unsignedIntegerValue] != [productID unsignedIntegerValue])
					match = NO;
			}
			
			if (key == XENControllerVendorIDKey)
			{
				NSNumber *vendorID = matchingDictionary[key];
				if ([deviceVendorID unsignedIntegerValue] != [vendorID unsignedIntegerValue])
					match = NO;
			}
			
			if (key == XENControllerUsagePageKey)
			{
				NSNumber *usagePage = matchingDictionary[key];
				if ([devicePrimaryUsagePage unsignedIntegerValue] != [usagePage unsignedIntegerValue])
					match = NO;
			}
			
			if (key == XENControllerUsageKey)
			{
				NSNumber *usage = matchingDictionary[key];
				if ([devicePrimaryUsage unsignedIntegerValue] != [usage unsignedIntegerValue])
					match = NO;
			}
			
			if (key == XENControllerUsagePairsKey)
			{
				//NSDictionary *usagePairs = matchingDictionary[key];
				// TODO: Write me!
			}
		}
		
		// Return the first match we get.
		if (match == YES)
			return deviceClass;
	}
	
	// No match found.
	const NSString *vendor = (NSString *)CFBridgingRelease(IOHIDDeviceGetProperty(aDevice, CFSTR(kIOHIDManufacturerKey)));
	const NSString *name = (NSString *)CFBridgingRelease(IOHIDDeviceGetProperty(aDevice, CFSTR(kIOHIDProductKey)));
	NSLog(@"No profile match on device: %p \n\t Usage: %04lX:%04lX \n\t Name: %@ \n\t Manufacturer: %@", aDevice, [devicePrimaryUsagePage longValue], [devicePrimaryUsage longValue], name, vendor);
	
	return Nil;
}

@end
