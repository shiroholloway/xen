//
//  XENHIDManager.h
//  Xen
//
//  Created by Robert Luis Hoover on 10/8/13.
//  Copyright (c) 2013 ars draconis. All rights reserved.
//

@import Foundation;

/**
 *	<code>XENHIDManager</code> wraps a reference to an IOHIDManager and is used to get a list
 *	of connected HID devices. It is only meant to be used internally.
 */
@interface XENHIDManager : NSObject

// Retrieving the Shared Instance ----------------------------------------------
/**
 *	Returns the process's default instance of XENHIDManager.
 *
 *	@return The current process's shared instance of XENHIDManager.
 */
+ (XENHIDManager *)defaultManager;

// Getting Connected Devices ---------------------------------------------------
/**
 *	Returns an array containing instances of XENController for all HID devices
 *	connected to the system.
 *
 *	@return An NSArray of all HID devices connected to the system.
 */
- (NSArray *)allDevices;

/**
 *	Returns an array containing instances of XENController for all devices
 *	connected to the system that match the input criteria.
 *
 *	@param matchingDictionary An NSDictionary containing device matching criteria.
 *
 *	@return An NSArray of HID devices matching the dictionary criteria.
 */
- (NSArray *)devicesMatchingDictionary:(NSDictionary *)matchingDictionary;

@end
