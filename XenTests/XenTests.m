//
//  XenTests.m
//  XenTests
//
//  Created by Robert Luis Hoover on 12/15/13.
//  Copyright (c) 2013 ars draconis. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface XenTests : XCTestCase

@end

@implementation XenTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end
