//
//  ViewLayerTests.m
//  Xen
//
//  Created by Robert Luis Hoover on 12/15/13.
//  Copyright (c) 2013 ars draconis. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "XENOpenGLView.h"
#import "XENOpenGLViewController.h"
#import "XENOpenGLViewDelegate.h"
#import "XENOpenGLViewControllerDelegate.h"


@interface ViewTests : XCTestCase

@end

// -----------------------------------------------------------------------------
// View Tests
// -----------------------------------------------------------------------------
@implementation ViewTests

// Housekeeping ----------------------------------------------------------------
#pragma mark - Housekeeping

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}


// View Tests ------------------------------------------------------------------
#pragma mark - View Tests

- (void)testView
{
	// TODO: Create a view and check that it works.
	// Make sure to test delegate functions.
    XCTFail(@"Unit tests are not implemented yet in XENInputTests");
}

- (void)testViewController
{
	// TODO: Create a view controller and mock view and make sure it works as expected.
	// Make sure to test delegate functions.
	XCTFail(@"Unit tests are not implemented yet in XENInputTests");
}

@end
