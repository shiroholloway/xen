//
//  InputLayerTests.m
//  Xen
//
//  Created by Robert Luis Hoover on 12/15/13.
//  Copyright (c) 2013 ars draconis. All rights reserved.
//

@import XCTest;
@import Foundation;

#import "XENHIDManager.h"
#import "XENController.h"

#import "XENKeyboard.h"
#import "XENMouse.h"
#import "XENPS3Controller.h"

@interface InputTests : XCTestCase

@end

// -----------------------------------------------------------------------------
// Input Tests
// -----------------------------------------------------------------------------
@implementation InputTests

// Housekeeping ----------------------------------------------------------------
#pragma mark - Housekeeping

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}


// Input Layer Foundation ------------------------------------------------------
#pragma mark - Input Layer Foundation

- (void)testHIDManager
{
	NSArray *devices;
	XENHIDManager *manager;
	
	// Getting the Default Manager
	manager = [XENHIDManager defaultManager];
	XCTAssertNotNil(manager, @"Could not get the default instance of XENHIDManager.");
	
	// Must pump the run loop to get devices registered.
	CFRunLoopRunInMode(kCFRunLoopDefaultMode, 0, false);
	
	// TODO: Test Notifications
	
	// Getting Connected Devices
	devices = [manager allDevices];
	XCTAssertNotNil(devices, @"XENHIDManager did not return an array.");
	XCTAssert([devices count], @"XENHIDManager returned an empty array.");
	XCTAssert(![devices respondsToSelector:@selector(addObject:)], @"XENHIDManager returned a mutable array.");
	
	// TODO: devicesMatchingDictionary
	// TODO: deviceMatchingHIDDevice
}

- (void)testController
{
	XENController *device;
	
	// Creation
	XCTAssertThrows(device = [[XENController alloc] initWithHIDDevice:NULL], @"A XENController was created with a NULL IOHIDDeviceRef.");
	XCTAssertNil(device, @"A XENController was created with a NULL IOHIDDeviceRef.");
		
	// Discovering Devices
	// Must pump the run loop to get devices registered.
	NSArray *devices = [XENController controllers];
	CFRunLoopRunInMode(kCFRunLoopDefaultMode, 0, false);
	
	devices = [XENController controllers];
	XCTAssertNotNil(devices, @"XENController did not return an array.");
	XCTAssert([devices count], @"XENController created an empty devices array.");
	
	// TODO: devicesSupportingProfile:.
}


- (void)testControllerProfile
{
	XENControllerProfile *profile;
	
	// Creation
	XCTAssertThrows(profile = [[XENControllerProfile alloc] initWithDevice:NULL], @"A XENInputProfile was created with a NULL IOHIDDeviceRef.");
	XCTAssertNil(profile, @"A XENInputProfile was created with a NULL IOHIDDeviceRef.");
}


// Device Profiles -------------------------------------------------------------
#pragma mark - Device Profiles

- (void)testKeyboard
{
#if 0
	XENController *keyboard = [[XENController devicesSupportingProfile:[XENKeyboard class]] lastObject];
	XCTAssertNotNil(keyboard, @"A keyboard device could not be created.");
	XCTAssert([keyboard.profile isMemberOfClass:[XENKeyboard class]], @"A keyboard device was initialized with the wrong profile.");
#endif
	// Uncomment to test value callback.
	//CFRunLoopRunInMode(kCFRunLoopDefaultMode, 5.0, false);
}

- (void)testMice
{
#if 0
	XENController *mouse = [[XENController devicesSupportingProfile:[XENMouse class]] lastObject];
	XCTAssertNotNil(mouse, @"A mouse device could not be created.");
	XCTAssert([mouse.profile isMemberOfClass:[XENMouse class]], @"A mouse device was initialized with the wrong profile.");
#endif
	// Uncomment to test value callback.
	//CFRunLoopRunInMode(kCFRunLoopDefaultMode, 5.0, false);
}

- (void)testPS3Controller
{
	// IDEA: Add a #ifdef to remove the alert when doing automated testing.
	NSAlert *alert = [NSAlert alertWithMessageText:@"Make sure a PS3 controller is connected to the computer."
									 defaultButton:nil
								   alternateButton:nil
									   otherButton:nil
						 informativeTextWithFormat:@"Running this test without a PS3 controller will result in automatic failure."];
	[alert setIcon:[NSImage imageNamed:NSImageNameCaution]];
	[[alert window] orderFront:nil];
	[alert runModal];
#if 0
	XENController *controller = [[XENController devicesSupportingProfile:[XENPS3Controller class]] lastObject];
	XCTAssertNotNil(controller, @"A PS3 controller device could not be created.");
	XCTAssert([controller.profile isMemberOfClass:[XENPS3Controller class]], @"A PS3 controller device was initialized with the wrong profile.");
	CFRunLoopRunInMode(kCFRunLoopDefaultMode, 5.0, false);
#endif
}

@end
