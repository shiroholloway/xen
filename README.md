Xen
===
An Objective-C game framework for Cocoa

## Overview
Xen attempts to make it easier to write games using Cocoa by handling the boring stuff. I'm currently working on it in my spare time, so progress is sporadic. Regardless, feel free to contribute! Pull requests are more than welcome.

Currently, there is no open source license picked out for the project.

## Components
### View
The view classes contain the boilerplate code required to set up a rendering loop in OS X. They are patterned after GLKView and GLKViewController in the iOS GLKit framework.

XENOpenGLView is a subclass of Cocoa's NSOpenGLView. It defines a delegate protocol to allow a delegate to draw the scene. By default, rendering is driven by Cocoa, although this can be customized by setting enableSetNeedsDisplay to NO and calling displayFrame: as necessary.

XENOpenGLViewController is a subclass of NSViewController. It drives the rendering of XENOpenGLView using a Display Link. It defines a delegate protocol to hook into, allowing you to update the scene and be notified when rendering is about to be paused/unpaused.

### Input
The input classes are designed to mimic the public API of Apple's Game Controller framework, to allow them to be used as drop-in replacements. It interfaces with OS X's HID Manager.

XENHIDManager contains an instance of an HID Manager. This class is meant to be used internally only.

XENController is a wrapper for an IOHIDDeviceRef. It corresponds to GCController in Cocoa's Game Controller framework.

XENControllerProfile maps the hardware controls of a particular input device to a set of elements from which you can read values. It is a class cluster. Subclasses contain implementation specific code for the device they represent; they live in /Input/Profiles. To implement new devices, subclass XENControllerProfile, implement input elements for each physical control you want to represent, and override the input: method.

XENControllerElement is an abstract base class for describing the elements (buttons, axes, etc.) that make up an input device. Subclasses live in /Input/Input Elements.

XENSnapshot is a class designed to encapsulate the input values of a device at a specific moment in time. Device-specific subclasses may be required. It remains to be implemented.

### Force Feedback
The force feedback component is low priority and has not been written (or even designed). An investigation into OS X's Force Feedback framework is necessary.

### Sound
Sound will be the next step after input is tackled. It has not been designed.


## To Do
### General
* Add a static library target for iOS support.
* Need to pick a license.
* Add a module map.

### Input
* There should only ever be one instance of a XENController per device. This will require refactoring of XENHIDManager to keep track of what devices have been given out.
* For settings LEDs on controllers, we'll need to be able to communicate with devices through XENController.
* It'd be nice to add support for the DualShock 4, Xbox 360 wired and wireless controllers, Wii Remotes, and the Wii U Pro Controller. Xbox 360 controller support will require a custom driver.